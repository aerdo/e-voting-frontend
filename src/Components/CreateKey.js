async function generateKeyPair() {
    let keyPair = await window.crypto.subtle.generateKey(
        {
            name: "ECDSA",
            namedCurve: "P-384"
        },
        true,
        ["sign", "verify"]
    );
    const publicKey = await window.crypto.subtle.exportKey("jwk", keyPair.publicKey);
    const publicKeyS = JSON.stringify(publicKey);

    const privateKey = await window.crypto.subtle.exportKey("jwk", keyPair.privateKey);
    const privateKeyS = JSON.stringify(privateKey);

    return [publicKeyS, privateKeyS];
}

export default generateKeyPair;