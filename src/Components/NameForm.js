import React, {useState} from "react";
import {Button, Form} from "react-bootstrap";
import generateKeyPair from "./CreateKey";
import PostData from "./PostData";
import {link} from "./Link";

function NameForm(){
    const [userName, setUserName] = useState('');
    const [message, setMessage] = useState('');
    const [show, setShow] = useState(false);

    const sub = () =>{
        if (userName.trim().length !== 0) {
            setMessage('');
            generateKeyPair().then((res) => {
                localStorage.setItem('protocol', `Пользователь ${userName} сгенерировал свою пару ключей.\n\n`);
                const data = {
                    name: userName,
                    publicKey: res[0]
                };
                const privateKey = res[1];
                PostData(data, `${link}/user`).then((resp)=>{
                    localStorage.setItem('protocol', localStorage.getItem('protocol') + `Открытый ключ пользователя ${userName} был отправлен в ЦИК...\n`);
                    if (resp.message === 'User created'){
                        localStorage.setItem('protocol',localStorage.getItem('protocol') +  `--- Открытый ключ пользователя ${userName} был сохранен в ЦИК.\n\n`);
                        document.cookie = `user&key=${userName}&${privateKey}`;
                        localStorage.setItem('voted', '0');
                        window.location.reload();
                    }else if (resp.message === 'Username must be unique'){
                        setMessage('Попробуйте другое имя пользователя');
                    }
                });
            })
        }else{
            setMessage('Введите имя');
            setUserName('');
        }
    }


    return(
        <div style={{width: "min(60vw, 500px)", height: "160px", margin: "calc(40vh - 80px) auto 0"}}>
            <h3 style={{textAlign: "center", marginBottom: "50px"}}>Голосование за лучшую фотографию</h3>
            <div className="name">
                <Form id="newUser">
                    <Form.Label>Чтобы проголосовать, введите Ваше имя:</Form.Label>
                    <Form.Control type="text" placeholder="Ваше имя" required value={userName}
                                  onChange={(e) => setUserName(e.target.value)}
                                  onKeyPress={(e)=> {
                                      if (e.key === "Enter") {
                                          e.preventDefault();
                                          sub();
                                      }
                                  }}
                    />
                </Form>
                <div style={{display: "inline-flex", alignItems: "center", marginTop: "15px"}}>
                    <Button variant="dark" form="newUser" onClick={()=>{sub()}}>Продолжить</Button>
                    <Form.Text style={{marginLeft: "20px"}}>
                        {message.length === 0 ? " " : message}
                    </Form.Text>
                </div>
            </div>

            <div style={{marginLeft: "15px"}}>
                <Form.Text onClick={()=>{
                    setShow(true);
                    setTimeout(()=>{setShow(false)}, 3000);
                }} style={{cursor: "pointer"}}>Информация об авторе</Form.Text>
                {show ? (<Form.Text style={{marginLeft: "15px"}}>Выполнила Гаврилова Дарья, группа А-05-18</Form.Text>) : (<></>)}
            </div>

        </div>

    )
}

export default NameForm;