import {link} from "./Link";

export default async function GetPubKey(){
    const response = await fetch(`${link}/publicKey`);
    return await response.json();
}
