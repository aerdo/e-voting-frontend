export default async function PostData(data, link){
    try {
        const response = await fetch(link,{
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return await response.json();
    }catch{
        return("Error");
    }
}




