/*export default */ async function EncryptData(data, publicKeyString){
    const pubKey = await importPublicKey(publicKeyString);
    let enc = new TextEncoder();
    const encData = enc.encode(JSON.stringify(data));
    return await window.crypto.subtle.encrypt(
        {
            name: "RSA-OAEP"
        },
        pubKey,
        encData
    );
}

export default async function GetEncryptedArray(data, publicKeyString){
    const encrypt = await EncryptData(data, publicKeyString);
    const view = new Uint8Array(encrypt);
    return Array.from(view);
}

async function importPublicKey(publicKeyString){
    const publicKeyJSON = JSON.parse(publicKeyString);
    return await window.crypto.subtle.importKey(
        "jwk",
        publicKeyJSON,
        {
            name: "RSA-OAEP",
            modulusLength: 4096,
            publicExponent: new Uint8Array([1, 0, 1]),
            hash: "SHA-256"
        },
        true,
        ["encrypt"]);
}