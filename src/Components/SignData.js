/*export default*/ async function SignData(data, privateKeyString){
    try{
        const privateKey = await importPrivateKey(privateKeyString);
        let enc = new TextEncoder();
        const encData = enc.encode(data);
        return await window.crypto.subtle.sign(
            {
                name: "ECDSA",
                hash: {name: "SHA-384"},
            },
            privateKey,
            encData
        );
    } catch {
        return "Error";
    }

}

export default  async function GetSignedArray(data, privateKeyString){
    const sign = await SignData(data, privateKeyString);
    if (sign === "Error"){
        return sign;
    }else{
        const view = new Uint8Array(sign);
        return Array.from(view);
    }

}

async function importPrivateKey(privateKeyString){
    const privateKeyJSON = JSON.parse(privateKeyString);
    return await window.crypto.subtle.importKey(
        "jwk",
        privateKeyJSON,
        {
            name: "ECDSA",
            namedCurve: "P-384"
        },
        true,
        ["sign"]);
}