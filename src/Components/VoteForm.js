import React, {useState} from "react";
import {Button, Spinner} from "react-bootstrap";
import GetPubKey from "./GetPubKey";
import GetSignedArray from "./SignData";
import GetEncryptedArray from "./EncryptData";
import PostData from "./PostData";

import pic1 from '../candidates/1.jpg';
import pic2 from '../candidates/2.jpg';
import pic3 from '../candidates/3.jpg';
import pic4 from '../candidates/4.jpg';

import {link} from "./Link";

function VoteForm() {
    const [res, setRes] = useState(0);
    const [isLoading, setLoading] = useState(false);
    const [voted, setVoted] = useState(localStorage.getItem('voted'));

    const downloadProtocol = () => {
        const data = localStorage.getItem('protocol');
        let file = new Blob([data], {type: 'application/json'});
        const element = document.getElementById("download");
        element.href = URL.createObjectURL(file);
        let date = new Date();
        element.download = `Протокол голосования ${
            date.getDate() + '.' +
            (date.getMonth() < 9 ?'0'+ (date.getMonth() + 1) : (date.getMonth() + 1)) + '.'
            + date.getFullYear() 
            + ' ' + date.getHours() + `:` + date.getMinutes()}.txt`;
    }

    if (voted === '0') {
        const str = document.cookie.split('user&key=')[1].split('&');
        const userName = str[0];
        const key = str[1];

        const refuse = () => {
            PostData({name: userName}, `${link}/delete-user`).then(()=>{
                localStorage.setItem('protocol',localStorage.getItem('protocol') + `Пользователь ${userName} отказался от голосования. Информация пользователя удалена из ЦИК.\n`);
                setVoted('3');
            })
        }

        const handleSubmit = () =>{
            setLoading(true);
            localStorage.setItem('protocol',localStorage.getItem('protocol') + `Пользователь ${userName} выбрал вариант №${res}.\n`);
            // подписывание бюллетени
            GetSignedArray(res, key).then((sign)=>{
                if (sign !== "Error"){
                    localStorage.setItem('protocol',localStorage.getItem('protocol') + `Пользователь ${userName} подписал бюллетень своим закрытым ключом.\n`);
                    const dataToSend = {
                        name: userName,
                        vote: res,
                        signature: sign
                    }
                    localStorage.setItem('protocol',localStorage.getItem('protocol') + `\nПользователь ${userName} запросил открытый ключ ЦИК...\n`);
                    GetPubKey().then((res)=>{
                        localStorage.setItem('protocol',localStorage.getItem('protocol') + `--- Открытый ключ ЦИК был получен.\n`);
                        const pubKey = res.key;
                        // шифрование бюллетени
                        GetEncryptedArray(dataToSend, pubKey).then((encryptedData)=>{
                            localStorage.setItem('protocol',localStorage.getItem('protocol') + `\nПользователь ${userName} зашифровал бюллетень открытым ключом ЦИК.\n`);
                            localStorage.setItem('protocol',localStorage.getItem('protocol') + `Пользователь ${userName} отправил бюллетень в ЦИК.\n\n`);
                            PostData(encryptedData, `${link}/vote`).then((voteRes)=>{
                                localStorage.setItem('protocol', localStorage.getItem('protocol') + `Пользователь ${userName} получил ответ от ЦИК...\n`);
                                localStorage.setItem('protocol', localStorage.getItem('protocol') +  '--- Сообщение было расшифровано и проверено.\n\n')
                                setLoading(false);
                                if (voteRes.message === 'Vote is counted'){
                                    localStorage.setItem('protocol',localStorage.getItem('protocol') + `Голос пользователя ${userName} учтен. Протокол завершен успешно.\n`);
                                    // завершение протокола
                                    localStorage.setItem('voted', '1')
                                    localStorage.setItem('protocol',localStorage.getItem('protocol') + `Пара ключей удалена.\n\n`);
                                    setVoted('1');
                                } else if (voteRes.message === 'Error-Sign') {
                                    localStorage.setItem('protocol', localStorage.getItem('protocol') + 'Произошла ошибка при проверке подлинности ЭП. Информация пользователя удалена из ЦИК.\n')
                                    setVoted('2');
                                }
                            })
                        })

                    })
                } else {
                    setLoading(false);
                    localStorage.setItem('protocol', localStorage.getItem('protocol') +  'Произошла ошибка при создании ЭЦП. Информация пользователя удалена из ЦИК.\n')
                    PostData({name: userName}, `${link}/delete-user`).then(()=>{
                        setVoted('2');
                    })
                }
            })
        }

        return (
            <>
                <table className="pic-container">
                    <tbody>
                    <tr>
                        <td colSpan="2" style={{textAlign: "center"}}>
                            <h4>Голосование</h4>
                            <p style={{margin: 0}}>{userName}, выберите лучшую фотографию из предложенных:</p>
                            <p style={{cursor: "pointer", color: "grey", textDecoration: "none", margin: 0}} onClick={()=>refuse()}>
                                Отказатья от голосования
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td><div id="1"  className={res===1 ? 'res' : ''} title="Кандидат №1" onClick={() => setRes(1)}
                            style={{backgroundImage: `url(${pic1})`,
                                backgroundRepeat: "no-repeat", backgroundPosition: "center center"}}/></td>
                        <td><div id="2" className={res===2 ? 'res' : ''} title="Кандидат №2" onClick={() => setRes(2)}
                                 style={{backgroundImage: `url(${pic2})`,
                                     backgroundRepeat: "no-repeat", backgroundPosition: "center center"}}/></td>
                    </tr>
                    <tr>
                        <td><div id="3" className={res===3 ? 'res' : ''} title="Кандидат №3" onClick={() => setRes(3)}
                                 style={{backgroundImage: `url(${pic3})`,
                                     backgroundRepeat: "no-repeat", backgroundPosition: "center center"}}/></td>
                        <td><div id="4" className={res===4 ? 'res' : ''} title="Кандидат №4" onClick={() => setRes(4)}
                                 style={{backgroundImage: `url(${pic4})`,
                                     backgroundRepeat: "no-repeat", backgroundPosition: "center center"}}/></td>
                    </tr>
                    <tr>
                        {res === 0 ? (<></>) : <td colSpan="2">
                            {isLoading ?
                                (
                                    <Button variant="dark" id="load">
                                        <Spinner
                                            as="span"
                                            animation="border"
                                            size="sm"
                                            role="status"
                                            aria-hidden="true"
                                        />
                                        <span className="visually-hidden">Loading...</span>
                                    </Button>
                                ) :
                                (
                                    <Button variant="dark" id="continue" onClick={() => handleSubmit()}>Проголосвать
                                        за {res}!</Button>
                                )
                            }
                        </td>}
                    </tr>
                    </tbody>
                </table>
            </>
        )
    }else if (voted === '1'){
        document.cookie = 'user&key=; expires=Thu, 01 Jan 1970 00:00:00 GMT;';
        localStorage.removeItem('voted');
        return (
            <div style={{textAlign: "center"}}>
                <h4 style={{marginTop: "43vh"}}>Спасибо, Ваш голос учтен!</h4>
                <a style={{cursor: "pointer", color: "black", textDecoration: "none", fontSize: "large"}} href={`${link}/results`} target="_blank">Результаты голосования</a>
                <a style={{cursor: "pointer", color: "grey", textDecoration: "none", display: "block"}} id="download" onClick={()=>{downloadProtocol()}}>Скачать протокол</a>
            </div>
        )
    } else if (voted === '2'){
        document.cookie = 'user&key=; expires=Thu, 01 Jan 1970 00:00:00 GMT;';
        localStorage.removeItem('voted');
        return (
            <div style={{textAlign: "center"}}>
                <h4 style={{marginTop: "43vh"}}>Что-то пошло не так. Повторите попытку</h4>
                <a style={{cursor: "pointer", color: "grey", textDecoration: "none"}} id="download" onClick={()=>{downloadProtocol()}}>Скачать протокол</a>
            </div>
        )
    } else if (voted === '3'){
        document.cookie = 'user&key=; expires=Thu, 01 Jan 1970 00:00:00 GMT;';
        localStorage.removeItem('voted');
        return (
            <div style={{textAlign: "center"}}>
                <h4 style={{marginTop: "43vh"}}>Жаль, что Вы отказались</h4>
                <a style={{cursor: "pointer", color: "grey", textDecoration: "none"}} id="download" onClick={()=>{downloadProtocol()}}>Скачать протокол</a>
            </div>
        )
    }
}
export default VoteForm;

