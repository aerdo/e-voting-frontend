import React, {useState, useEffect} from "react";
import './App.css';
import NameForm from "./Components/NameForm";
import VoteForm from "./Components/VoteForm";
import {link} from "./Components/Link";

function App() {
    document.title = "Электронное голосование";
    const [awake, setAwake] = useState(false);

    async function ping(){
        const response = await fetch(`${link}/publicKey`);
        return await response.json();
    }

    useEffect(()=>{
        if (!awake){
            ping().then(()=>{
                setAwake(true);
            })
        }
    })

    if (awake) {
        if (document.cookie.length === 0) {
            localStorage.removeItem('protocol');
            return (
                <NameForm/>
            );
        } else {
            return (<VoteForm/>)
        }
    }
}

export default App;
